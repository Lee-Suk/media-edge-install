1. 사전 준비사항
   1. etc/hosts 설정
   2. coredns 설정
   3. vmmax 설정(elasticsearch, opensearch 사용시 필수 적용 필요)
      ```shell
      echo "vm.max_map_count=262144" >> /etc/sysctl.conf 
      sysctl -p
      ```

2. argo cd 설치
```shell
helm repo add --insecure-skip-tls-verify --username suser --password gozldqkdwl2 infra https://ntels.harbor.core/chartrepo/infra
helm repo update
helm -n infra-deploy upgrade -i --create-namespace --insecure-skip-tls-verify infra-deploy infra/infra-deploy --set jenkins.enabled=false
```

2. argocd password 확인
```shell
 kubectl -n infra-deploy get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' |base64 -d
```

3. helm repo cert 설치
```shell
kubectl apply -f repo-cert.yaml
```

4. helm repo 등록
```shell
kubectl apply -f repo-media-edge-helm.yaml
kubectl apply -f repo-infra-helm.yaml
kubectl apply -f repo-media-edge-git.yaml
```

5. media-edge helm 설치 
 - infra-metric의 경우 kube-prometehus-stack이 설치되어 있으면 설치 불가
```shell
kubectl apply -f app-helm-infra-base.yaml
# kubectl apply -f app-helm-infra-metric.yaml
kubectl apply -f app-helm-infra-monitor.yaml
kubectl apply -f app-helm-infra-log.yaml
kubectl apply -f app-helm-media-edge-base.yaml
kubectl apply -f app-git-media-edge-deploy.yaml
```

4. UI 접속후 application sync
   - port-forward 설정
   ```shell
   kubectl -n infra-deploy port-forward service/infra-deploy-argocd-server 8080:80
   ```
   - browser에서 localhost:8080 으로 접속 

* 참고 사항
  - infra-metric의 경우 Degraded 시 추가 sync 필요
  - kube-prometheus-stack([link](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)) 이 이미 설치되어있을 경우 infra-metric 설치 불가
  - kube-prometehus-stack의 위치가 다를경우 grafana data-source 위치 설정 필요